import express from 'express';
const WebSocket = require('ws')
const app = express();
const port = 3000;

const wss = new WebSocket.Server({ port: 3001 })

wss.on('connection', ws => {
  ws.on('message', message => {
    console.log(`Received message => ${message}`)
  })
  ws.send('Update from trello server.');
})

app.use(express.static(__dirname, { dotfiles: 'allow' } ));

app.get('/', (req, res) => {
    console.log('Get Request.');
    res.send('Hello World!');
  })
  
app.listen(port, () => {
  console.log(`Timezones by location application is running on port ${port}.`);
});